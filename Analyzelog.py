#  this will open the claritylog file and count the  number of
#    gets   ("timing.http.get")
#    posts  ("timing.http.post")
#    clarity.grails.StatusService
#   email_event
#   Posting routing XML
#   submission
#    Receive Samples From Barcode Association
#     couchdb.grails.CouchDbService
import datetime
from datetime import timedelta


class AnalyzeLog():
    def __init__(self):
        self.errorCnt = 0
        self.startDate = datetime.datetime.now()
        self.lastDate = datetime.datetime.now()
        self. wordArray = ['timing.http.get','timing.http.post',
                     'clarity.grails.StatusService',
                     'email_event','Posting routing XML',
                     'submission',
                     'Receive Samples From Barcode Association',
                     'couchdb.grails.CouchDbService',
                     'submitted-by']

    # -----------------------------------------------------
    # validate that the input text is a valid date
    def validate(self,lineText):
        status=True
        lineText = lineText + '000'
        try:
            datetime.datetime.strptime(lineText, '%Y-%m-%d %H:%M:%S.%f')
        except (ValueError):    #not a date
            status = False
        return status

    # -----------------------------------------------------
    # get the first date logged in the log file
    def getStartDate(self):
        for i, line in enumerate(self.lines):
            timeDate = line.split()
            if len(timeDate) > 1:
                validDate = timeDate[0] + " " + timeDate[1]
                if self.validate(validDate):
                    self.startDate = datetime.datetime.strptime(validDate, '%Y-%m-%d %H:%M:%S.%f')
                    return True
        return False


    # -----------------------------------------------------
    # give a start time and an end time, return the portion of the logfile as a list of
    # lines
    def getLogFilePortion(self, sTime, eTime, logFile):

        firstLine = self.getLineNumber(logFile,sTime,"Begin")
        lastLine = self.getLineNumber(logFile,eTime,"End")
        if (firstLine<0):
            print ("***Error - start Time specified not in file, setting to first entry of logfile")
            firstLine = 0
        if  (lastLine<0):
            print("***Error - End Time specified not in file, setting to last entry of logfile")
            lastLine = len(self.lines) -2

        print ("logfile portion lines: ",firstLine,":", lastLine)
        portion = logFile[firstLine:lastLine]
        return portion

    # -----------------------------------------------------
    # get the line number of the 1st line that matches the logdate.
    # inputs:  logfile  (list of lines that store the logfile content
    #          logdate  - the date that you are searching the file for
    #          filePosition - either "Begin" or "End" to indicate if line number found is for 1st or last
    def getLineNumber(self,logFile,logdate,filePosition):

        for i, line in enumerate(logFile):
            timeDate = line.split()
            if len(timeDate) > 1:
                lineDate = timeDate[0] + " " + timeDate[1]
                if self.validate(lineDate):  #is this a valid date text
                    dateToCheck = datetime.datetime.strptime(lineDate, '%Y-%m-%d %H:%M:%S.%f')
                    if (dateToCheck >=logdate):
                            print(filePosition , " date found:", dateToCheck, " on line number: ", i)
                            return i


        return -1


    # -----------------------------------------------------
    # analyze the file from the startTime to endTime
    # this will open the log file, determine the portion of log file to analyze
    # find the # of occurances of the words of  self.wordArray in the log file portion
    def AnalyzeRange(self,startTime, endTime):
        filename = "C:\\tmp\\clarity-scripts.log"
        sTime = datetime.datetime.strptime(startTime, '%Y-%m-%d %H:%M:%S')
        eTime = datetime.datetime.strptime(endTime, '%Y-%m-%d %H:%M:%S')
        # read the file into a list of lines
        with open("C:\\tmp\\clarity-scripts.log", 'r') as f:
            self.lines = f.read().split("\n")

        logfilePortion = self.getLogFilePortion(sTime,eTime,self.lines)

        print("Number of lines is {}".format(len(logfilePortion)))
        print ("first line:")
        print (logfilePortion[0])
        self.countOccurances(logfilePortion)  #get the number of occurrances for each search in log
        print("last line:")
        print (logfilePortion[len(logfilePortion)-2])
        f.close()

    # -----------------------------------------------------
    # count the number of each of the words (from self.wordarray) in the
    # log file
    def countOccurances(self,logFile):
        print("======================================================")

        for word in self.wordArray:
            count=0
            for i, line in enumerate(logFile):
                if word in line:
                    count=count + 1
            print ("'" + word + "' found " + str(count) + " times")
        print("=======================================================")

    # -----------------------------------------------------
    # analyze the entire file from the first line to last line
    # this will open the log file,
    # find the # of occurances of the words of  self.wordArray in the log file
    def AnalyzeEntireFile(self):
        #print("Open file for reading")
        filename = "C:\\tmp\\clarity-scripts.log"

        # read the file into a list of lines
        with open("C:\\tmp\\clarity-scripts.log", 'r') as f:
            self.lines = f.read().split("\n")

        if (self.getStartDate()):
            print("Number of lines is {}".format(len(self.lines)))
            print ("first line:")
            print (self.lines[0])
            self.countOccurances(self.lines)  #get the number of occurrances for each search in log
            print("last line:")
            print (self.lines[len(self.lines)-2])
        f.close()
# -------------------------------------------------------------------------------------------
# run tests

myTest = AnalyzeLog()
print ("-----------Analyzing entire log file--------------------------------------------")
myTest.AnalyzeEntireFile()
print ("---------------done-------------------------------------")
print ("")
print ("")
print ("------------Analyzing partial log file-------------------------------------------")
# put in range of time to analyze  must be format: yyyy-mm-dd hh:mm:ss
startTime = "2018-11-08 00:01:00"
endTime = "2018-11-08 12:21:39"
myTest.AnalyzeRange(startTime,endTime)
print ("----------------done-------------------------------------")


errs = myTest.errorCnt

print("---Number of Errors Found = " + str(errs) + " ---")
